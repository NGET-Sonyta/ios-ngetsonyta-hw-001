//
//  ViewController.swift
//  MyApp
//
//  Created by Mavin on 9/7/20.
//  Copyright © 2020 hrd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Number showing on Label Result
    var runningNumber:String = ""
    
    //First Value gets from Label Result
    var firstValue:String = ""
    
    //Second Value gets from Label Result
    var secondValue:String = ""
    
    //Set result to 0.0
    var result:Double = 0
    
    //Set operator to false before clicking
    var hasOperator = false
    
    
    //Declare outlet label showing when users click on number buttons
    @IBOutlet weak var label: UILabel!
    
    //Declare outlet small red labels
    @IBOutlet weak var subResult: UILabel!
    
    //Declare small operators
    @IBOutlet weak var method: UILabel!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //Button equal get clicked
    @IBAction func equalButton(_ sender: UIButton) {
       
        secondValue = runningNumber
        
        //Checks if users skip on clicking numbers
        if firstValue == nil || firstValue == "" {
            firstValue = "0"
        }
        let sign = method.text!
        
        //check with mathmatics operators
        switch sign {
        case "+":
            result = Double(firstValue)! + Double(secondValue)!
        case "-":
            result = Double(firstValue)! - Double(secondValue)!
        case "x":
            result = Double(firstValue)! * Double(secondValue)!
        case "÷":
            result = Double(firstValue)! / Double(secondValue)!
        case "%":
            result = Double(Int(firstValue)! % Int(secondValue)!)
        default:
            print("Default")
        }
        
        var sepa = String(result).components(separatedBy: ".")
        
        //Check if result reachs infinity
        if result.isInfinite {
            label.textColor = .red
            label.text = "Infinity"
            
        //Cut "0" after dot sign
        }else if sepa[1] == "0" {
            label.text = String(sepa[0])
        }
        else {
            label.text = String(result)
           
        }
        subResult.text = secondValue
        method.text = ""
        runningNumber = ""
    }
    
    //Button +/- get clicked
    @IBAction func negativeNum(_ sender: UIButton) {
        
        method.text = sender.currentTitle
        let getVal = Double(label.text!)!
        let prevent = String(getVal * (-1)).components(separatedBy: ".")
        
        //Hide 0 after "."
        if prevent[1] == "0" {
            label.text = prevent [0]
        }
        
    }
    
    //Button Clear(C) get clicked
    @IBAction func clearButton(_ sender: UIButton) {
        
        //Clear everything
        subResult.text! = "0"
        firstValue = ""
        secondValue = ""
        method.text = ""
        label.text = "0"
        runningNumber = ""
                    
    }
    
    //Button Dot(.) get clicked
    @IBAction func dot(_ sender: UIButton) {
        
        //Allow to input less than 8 digits
        if runningNumber.count <= 7 {
            
            //Check on input whether or not it has dot(.) in string
            if !runningNumber.contains("."){
                runningNumber = label.text == "0" ? "0." : label.text! + "."
                label.text = runningNumber
            }
        }
        
    }
    
    //Button numbers get clicked
    @IBAction func numbers(_ sender: UIButton) {
        
        label.textColor = .black
        
        //Set "0" to no value
        if runningNumber == "0"  {
            runningNumber = ""
        }
        //Allow to input no more than 8 digits
        if runningNumber.count <= 8 {
            if hasOperator {
                runningNumber = String(sender.tag)
                hasOperator = false
            }else {
                runningNumber += "\(sender.tag)"
            }
            label.text = runningNumber
        }
    }
    
    //Buttons get clicked
    @IBAction func buttons(_ sender: UIButton)
    {
        hasOperator = true
        firstValue = "0"
        secondValue = "0"
        
        //Stores number input as first value
        if label.text != "" {
            firstValue = label.text!
        }
    
        method.text = sender.currentTitle!
        subResult.text! = String(label.text!)
        firstValue = subResult.text!
        runningNumber = "0"
        
    }
        
    

}
